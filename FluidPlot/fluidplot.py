__author__ = 'vector'
import sys
from pylab import *
import matplotlib.animation as anim
import time

import yslices

fig, ax = plt.subplots()
#colorbar(shrink=.85)
im = ax.imshow(yslices.densityYSlice[0],interpolation='nearest', cmap='binary')

def initDensityAnim():
    imshow([[0]],interpolation='nearest', cmap='binary')
    pass

def animDensityFunc(i):
    im.set_data(yslices.densityYSlice[i])
    #imshow(yslices.densityYSlice[i],interpolation='nearest', cmap='binary')

def animDensity():
    ani = anim.FuncAnimation(fig, func=animDensityFunc, frames=np.arange(900), repeat=False )
    ani.save('density.avi', frame_prefix="-vcodec libx264 -vpre ultrafast -crf 15 -an", fps=30 )#, extra_args=['-vcodec', 'libx264'])
    #show(ani)

def animVel():
    fig = figure()
    dt = 0.01
    slice = yslices.velocityYSlice[0]
    q = quiver(slice[:,:,0], slice[:,:,2])#, scale=dt)
    def initAnim():
        q.set_UVC([[0,0,0]],[[0,0,0]],[0,0,0])
    def quiverAnim(i):
        slice = yslices.velocityYSlice[i]
        q.set_UVC(slice[:,:,0], slice[:,:,2], slice )
    ani = anim.FuncAnimation(fig, quiverAnim, np.arange(900), init_func=initAnim, repeat=False )
    #show(ani)
    ani.save('vel.avi',  frame_prefix="-vcodec libx264 -vpre ultrafast -crf 15 -an", fps=30)#, extra_args=['-vcodec', 'libx264'])
    
if __name__ == "__main__":
    print "Fluid Plot"

    animVel()
    animDensity()

