#include "Display.hpp"
#include "Renderable.hpp"
#include "RenderContext.hpp"
#include "Viewport.hpp"

#include "Mesh.hpp"
#include "SlicedVolume.hpp"
#include "RenderContext.hpp"
#include "Fluid.hpp"
#include "RayCastVolume.hpp"

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <AntTweakBar.h>

#include <memory>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <stdlib.h>

using namespace std;

Display* g_display;
TwBar *bar;         // 2d GUI overlay

class InteractionVars
{
public:
    InteractionVars()
    : vorticity( 300.0 ),
      absorption( 0.85 ),
      gravity(0.5 ),
      numSamples( 96 ),
      numLightSamples( 72 ),
      solverIterations( 5 ),
      fps( 0 ),
      rotationRate( 1.0f ),
      isSavingFrames( false ),
      scalarDataSource( RayCastVolume::DensityScalar ),
      vectorDataSource( RayCastVolume::NoneVector )
    {
        lightPos[0] = 0.0f;
        lightPos[1] = 0.5f;
        lightPos[2] = 0.5f;
        lightColor[0] = 0.7f;
        lightColor[0] = lightColor[0] = 0.3;
    }
    float vorticity;
    float absorption;
    float gravity;
    float lightPos[3];
    float lightColor[3];
    
    int numSamples;
    int numLightSamples;
    int solverIterations;
    float fps;
    float rotationRate;
    bool isSavingFrames;
    RayCastVolume::ScalarDataSource scalarDataSource;
    RayCastVolume::VectorDataSource vectorDataSource;
};

void GLFWCALL resizeWindowCallback( int width, int height )
{
    if( g_display )
    {
        g_display->resizeWindow( width, height );
    }
    // Send the new window size to AntTweakBar
    TwWindowSize(width, height);
}

void TW_CALL getFluidSizeCB( void* val, void* clientData )
{
    FluidPtr fluid = *((FluidPtr*)(clientData)) ;
    int* intPtr = (int*)val;
    *intPtr = fluid->getSize();
}
void TW_CALL setFluidSizeCB( const void* value, void* clientData )
{
    FluidPtr fluid = *((FluidPtr*)(clientData)) ;
    int size = *((const int*)(value));
    fluid->setSize( size );
}

/// Startup OpenGL and create the rendering context and window.
int initOpenGL( InteractionVars* vars, FluidPtr fluid )
{
    cerr << "glfwInit...";
    if( !glfwInit() )
    {
        cerr << "Failed to initialize GLFW.\n";
    }
    cerr << "done.\n";

    // OpenGL 3.2 or higher only
    glfwOpenWindowHint( GLFW_FSAA_SAMPLES, 16 ); // 8x anti aliasing
#ifdef __APPLE__
    // Need to force the 3.2 for mac -- note
    // that this breaks the AntTweak menus
    glfwOpenWindowHint( GLFW_OPENGL_VERSION_MAJOR, 3 );
    glfwOpenWindowHint( GLFW_OPENGL_VERSION_MINOR, 2 );
#endif
    glfwOpenWindowHint( GLFW_WINDOW_NO_RESIZE, GL_FALSE );
    
    cerr << "glfwOpenWindow...";
    glfwOpenWindow( 800, 400, 0, 0, 0, 0, 32, 0, GLFW_WINDOW ); //
    glfwEnable(GLFW_MOUSE_CURSOR);
    glfwSetWindowTitle( "softTest" );

    cerr << " done.\n";
    cerr << "glewInit... ";
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if( err != GLEW_OK )
    {
        cerr << "glewInit() failed!\n";
        return -1;
    }
    if( !glewIsSupported("GL_VERSION_3_2") )
    {
        cerr << "OpenGL Version 3.2 Required!\n";
        return -1;
    }
    if( GL_MAX_3D_TEXTURE_SIZE < 256 )
    {
        cerr << "Max 3d texture size: " << GL_MAX_3D_TEXTURE_SIZE << " is too small for this program.\n";
        return -1;
    }
    
    if (!TwInit(TW_OPENGL, NULL)) {
        std::cerr << "AntTweakBar initialization failed: \"" << TwGetLastError() << "\"\n";
    }
    // Create a tweak bar
    bar = TwNewBar("DBM in Fluid");
    TwDefine(" GLOBAL help='Fluid and spark sim.\n' "); // Message added to the help bar.
    TwAddVarRO(bar, "FPS", TW_TYPE_FLOAT, &(vars->fps), " label='FPS' ");
	TwAddVarRW(bar, "Vorticity", TW_TYPE_FLOAT, &(vars->vorticity), " label='Vorticity' min=0 max=5000 step=5 keyIncr=v keyDecr=V ");
    TwAddVarRW(bar, "Absorption", TW_TYPE_FLOAT, &(vars->absorption), " label='Absorption' min=0 max=1.5 step=0.05 keyIncr=a keyDecr=A ");
    TwAddVarRW(bar, "Gravity", TW_TYPE_FLOAT, &(vars->gravity), " label='Gravity' min=0 max=100.0 step=0.5 ");
    TwAddVarRW(bar, "Light Position - X", TW_TYPE_FLOAT, &(vars->lightPos[0]), " label='Light X' min=-10 max=10 step=0.1 ");
    TwAddVarRW(bar, "Light Position - Y", TW_TYPE_FLOAT, &(vars->lightPos[1]), " label='Light Y' min=-10 max=10 step=0.1 ");
    TwAddVarRW(bar, "Light Position - Z", TW_TYPE_FLOAT, &(vars->lightPos[2]), " label='Light Z' min=-10 max=10 step=0.1 ");

    TwAddVarRW(bar, "Light Color", TW_TYPE_COLOR3F, &(vars->lightColor), " label='Light Color' ");
    
	TwAddVarRW(bar, "Samples", TW_TYPE_INT32, &(vars->numSamples), " label='Number Samples' min=0 max=1000 step=1 ");
    TwAddVarRW(bar, "Light Samples", TW_TYPE_INT32, &(vars->numLightSamples), " label='Number Light Samples' min=0 max=1000 step=1 ");
	TwAddVarRW(bar, "Solver Iterations", TW_TYPE_INT32, &(vars->solverIterations), " label='Solver Iterations' min=1 max=50 step=1 ");
    TwAddVarRW(bar, "Saving Frames to Files", TW_TYPE_BOOLCPP, &(vars->isSavingFrames), " true=SAVING false=Disabled ");

    TwAddVarRW(bar, "Rotation Rate", TW_TYPE_FLOAT, &(vars->rotationRate), " label='Rotation Rate' min=0 max=5.0 step=0.1 ");
    TwAddVarCB(bar, "Resize Fluid", TW_TYPE_INT32, setFluidSizeCB, getFluidSizeCB, (void*)&fluid, " label='Fluid Size' min=6 max=64 step=1 " );

    
    TwType scalarDataSourceType = TwDefineEnumFromString( "ScalarDataSource", "None, Density, Vorticity Mag" );
    TwType vectorDataSourceType = TwDefineEnumFromString( "VectorDataSource", "None, Velocity, Vorticity, VorticityForce" );
    TwAddVarRW(bar, "Scalar Data", scalarDataSourceType, &(vars->scalarDataSource), NULL );
    TwAddVarRW(bar, "Vector Data", vectorDataSourceType, &(vars->vectorDataSource), NULL );

    // Set GLFW event callbacks
    // - Redirect window size changes to the callback function WindowSizeCB
    glfwSetWindowSizeCallback(resizeWindowCallback);
    // - Directly redirect GLFW mouse button events to AntTweakBar
    glfwSetMouseButtonCallback((GLFWmousebuttonfun)TwEventMouseButtonGLFW);
    // - Directly redirect GLFW mouse position events to AntTweakBar
    glfwSetMousePosCallback((GLFWmouseposfun)TwEventMousePosGLFW);
    // - Directly redirect GLFW mouse wheel events to AntTweakBar
    glfwSetMouseWheelCallback((GLFWmousewheelfun)TwEventMouseWheelGLFW);
    // - Directly redirect GLFW key events to AntTweakBar
    glfwSetKeyCallback((GLFWkeyfun)TwEventKeyGLFW);
    // - Directly redirect GLFW char events to AntTweakBar
    glfwSetCharCallback((GLFWcharfun)TwEventCharGLFW);
    
    glEnable( GL_DEPTH_TEST );
    cerr << "done.\n";
    return 0;
}

void setupSimpleDisplay( RenderContext& renderContext )
{
    delete g_display;
    g_display = new SimpleDisplay(renderContext);
    int height = 0; int width = 0;
    glfwGetWindowSize( &width, &height );
    g_display->resizeWindow( width, height );
}

void setup3dDisplay( RenderContext& renderContext )
{
    delete g_display;
    g_display = new SideBySideDisplay(renderContext);
    int height = 0; int width = 0;
    glfwGetWindowSize( &width, &height );
    g_display->resizeWindow( width, height );
}


/// Write a 24-bit color binary PPM image file for the current frame buffer
/// files are named sequentially starting at 1, padded to 4 digits.
void writeFrameBufferToFile( const std::string& frameBaseFileName ) 
{
    static unsigned int frameNumber = 1;
    
    int width = 0;
    int height = 0;
    glfwGetWindowSize( &width, &height );

    unsigned char* frameBuffer = new unsigned char[ 3 * width * height * sizeof(unsigned char) ];

    glPixelStorei( GL_PACK_ALIGNMENT, 1 ); // align start of pixel row on byte

    std::stringstream frameFileName;
    frameFileName << frameBaseFileName << std::setfill('0') << std::setw(4) << frameNumber++ << ".ppm";
    std::ofstream frameFile( frameFileName.str().c_str(), std::ios::binary | std::ios::trunc );
    glReadBuffer( GL_FRONT ); 
    glReadPixels( 0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, frameBuffer );

    // PPM header.  P6 is binary RGB
    frameFile << "P6\n" << width << " " << height << "\n255\n";
    for( int j = height-1; j>=0; --j )  // opengl vs image is swapped top-bottom
    {
        for( int i = 0; i < width; ++i )
        {
            frameFile << (char)frameBuffer[3*width*j + 3*i + 0] 
            << (char)frameBuffer[3*width*j + 3*i + 1] 
            << (char)frameBuffer[3*width*j + 3*i + 2] 
            ;
        }
    }
    frameFile.close();
    delete[] frameBuffer;
}

/// Display the fluid volume without simulating
int runDisplay(int argc, char** argv)
{
    InteractionVars vars;

    FluidPtr fluid( new Fluid() );
    fluid->loadFromFile( "test.fluid" );
    fluid->saveToFile( "second.fluid" );

    int retVal = initOpenGL(&vars, fluid);
    if( retVal ) return retVal;

    //RenderablePtr smokeVolume( new SlicedVolume(1024, fluid) );
    RenderablePtr smokeVolume( new RayCastVolume(fluid) );

    const float dt = 0.01;
    smokeVolume->update(dt);

    RenderContext renderContext;

    float angle = 160.0f;
    glm::mat4 trans;
    bool isWritingToFile = false;
    while( glfwGetWindowParam( GLFW_OPENED ) )
    {
        angle += 1.0f;
        trans = glm::rotate( glm::mat4(), angle, glm::vec3( 0.0f, 0.0f, 1.0f ) );

        glClearColor( 0.4, 0.4, 0.4, 1 );
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

        smokeVolume->render( renderContext );

        glfwSwapBuffers();

        if( isWritingToFile )
        {
            writeFrameBufferToFile( "smokeRender_" );
        }
        if( glfwGetKey( GLFW_KEY_ENTER ) == GLFW_PRESS )
        {
            // Reload shaders from files
            smokeVolume->loadShaders();
        }
        if( glfwGetKey( GLFW_KEY_ESC ) == GLFW_PRESS )
        {
            break;
        }
        if( angle > (160.0f + 360.f) )
        {
            break;
        }
        glfwSleep( 0.01 );
    }
    glfwTerminate();
    return 0;
}

/// Online simulation and display of fluid
int runSimulation(int argc, char** argv)
{
    using namespace std;
    InteractionVars vars;

    RenderContext renderContext;

#ifdef DEBUG
    const int sliceDim = 16;
#else
    const int sliceDim = 36;
#endif
    FluidPtr fluid( new Fluid( sliceDim ) );
    //vars.numSamples = sliceDim * 2.0;
    //vars.numLightSamples = sliceDim * 1.0;

    int retVal = initOpenGL( &vars, fluid );
    if( retVal ) return retVal;

    setupSimpleDisplay(renderContext);
    //setup3dDisplay(renderContext);

    Renderables scene;

    RayCastVolume* rayCastVolume = new RayCastVolume(fluid);
    RenderablePtr smokeVolume( rayCastVolume );
    scene.push_back( smokeVolume );

    
    float angle = 0.0f;
    bool isWritingPythonSlices = false;

    int sliceNumber = 0;
    std::ofstream pythonSlices;
    std::string sliceBaseName("densityYSlice");
    std::string velSliceBaseName( "velocityYSlice");
    if( isWritingPythonSlices )
    {
#ifdef WIN32
        pythonSlices.open("FluidPlot/yslices.py");
#else
        pythonSlices.open("../../FluidPlot/yslices.py");
#endif
        pythonSlices << "from numpy import array, float32\n\n" 
            << sliceBaseName << " = {}\n"
            << velSliceBaseName << " = {}\n";
    }

    const double startTime = glfwGetTime();
    double currTime = startTime;
    double lastTime = startTime;
    while( glfwGetWindowParam( GLFW_OPENED ) )
    {
        lastTime = currTime;
        currTime = glfwGetTime();
        vars.fps = 1.0f/(currTime - lastTime);
        const float dt = 0.01;
        angle += vars.rotationRate;
        renderContext.setModelMatrix(
            glm::translate( glm::rotate( glm::mat4(), angle, glm::vec3( 0.0f, 0.0f, 1.0f ) ), glm::vec3(-0.5, -0.5, -0.5) )
            );
        renderContext.setLightPosition( vars.lightPos );
            
        rayCastVolume->selectScalarDataSource( vars.scalarDataSource );
        rayCastVolume->selectVectorDataSource( vars.vectorDataSource );
        rayCastVolume->setVolumeSamples( vars.numSamples );
        rayCastVolume->setLightSamples( vars.numLightSamples );
            
        fluid->setSolverIterations( vars.solverIterations );
        fluid->setVorticity( vars.vorticity );
        fluid->setAbsorption( vars.absorption );
        fluid->setGravityFactor( vars.gravity );

        fluid->update(dt);        // compute new data for shader, displayed later

        smokeVolume->update(dt);  // copy data to graphics card

        if( isWritingPythonSlices )
        {
            fluid->writeYDensitySliceToPythonStream( pythonSlices, sliceBaseName, sliceNumber, sliceDim/2 );
            fluid->writeYVelocitySliceToPythonStream( pythonSlices, velSliceBaseName, sliceNumber, sliceDim/2, dt );
            sliceNumber++;
            pythonSlices.flush();
        }

        g_display->render( scene );
            
        if( vars.isSavingFrames ) writeFrameBufferToFile( "smoke_" );
        

        //////////////////////////////////////////////////////////////////////////
        // Process Inputs
        if( glfwGetKey( GLFW_KEY_ENTER ) == GLFW_PRESS )
        {
            smokeVolume->loadShaders();
        }
        if( glfwGetKey( GLFW_KEY_ESC ) == GLFW_PRESS )
        {
            break;
        }
        if( glfwGetKey( GLFW_KEY_BACKSPACE ) == GLFW_PRESS )
        {
            std::cerr << "Reset fluid.\n";
            fluid->reset();
        }
        if( glfwGetKey( GLFW_KEY_F2 ) == GLFW_PRESS )
        {
            std::cerr << "Switching to 3d\n";
            setup3dDisplay(renderContext);
        }
        if( glfwGetKey( GLFW_KEY_F1 ) == GLFW_PRESS )
        {
            std::cerr << "Switching to simple\n";
            setupSimpleDisplay(renderContext);
        }

        const float eyeSeparationStep = 0.02;
        if( glfwGetKey( GLFW_KEY_UP ) == GLFW_PRESS )
        {
            SideBySideDisplay* d = dynamic_cast<SideBySideDisplay*>(g_display);
            if( d )
            {
                float dist = d->getEyeSeparation() + eyeSeparationStep;
                d->setEyeSeparation( dist );
                std::cerr << "Increasing interoccular distance to " << dist << "\n";
            }
        }
        if( glfwGetKey( GLFW_KEY_DOWN ) == GLFW_PRESS )
        {
            SideBySideDisplay* d = dynamic_cast<SideBySideDisplay*>(g_display);
            if( d )
            {
                float dist = d->getEyeSeparation() - eyeSeparationStep;
                d->setEyeSeparation( dist );
                std::cerr << "Decreasing interoccular distance to " << dist << "\n";
            }
        }
        TwRefreshBar(bar); //< force refresh to update stats (like FPS) every frame
        TwDraw();
        glfwSwapBuffers();
    }
    TwTerminate();
    glfwTerminate();
    return 0;
}

/// Run a no-graphics timing loop and write to timings.txt file
/// Writes the resulting fluid density map to test.fluid
int runTiming( int argc, char* argv[] )
{
    // GLFW only used for high-precision timer
    if( !glfwInit() )
    {
        cerr << "Failed to initialize GLFW.\n";
    }

    const size_t sliceDim = 64;
    const size_t solverIters = 10;
    const size_t stepCount = 5;
    const size_t iterCount = 1;

    Fluid* fluid = new Fluid( sliceDim );
    fluid->setSolverIterations( solverIters );

    const float dt = 0.01;

    const double startTime = glfwGetTime();
    for( size_t i=0; i<iterCount; ++i )
    {
        for( size_t j=0; j<stepCount; ++j )
        {
            fluid->update(dt);
        }
    }
    const double elapsed = glfwGetTime() - startTime;

    fluid->saveToFile( "test.fluid" );

    std::ofstream timings( "timings.txt", std::ios::app );
    timings <<
#ifdef _DEBUG
    "Debug\t"
#else
    "Release\t"
#endif
    << elapsed << '\t' << (elapsed)/(double)(stepCount*iterCount) << '\t' << stepCount << '\t' << iterCount << '\t' << sliceDim << '\t' << solverIters << '\t' << dt << std::endl;
    std::cerr << "Average step time: " << (elapsed)/(double)(stepCount*iterCount) << "\nTiming run complete.\n";

    glfwTerminate();
    return 0;
}


int main( int argc, char* argv[] )
{
    g_display = NULL;
    return runSimulation( argc, argv );
    //return runTiming( argc, argv );
    //return runDisplay( argc, argv );
}

