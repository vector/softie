#ifndef MESH_HPP
#define MESH_HPP

#include "Renderable.hpp"
#include "RenderContext.hpp"

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glfw.h>

//#include <SOIL.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>

#include <memory>
#include <iostream>
#include <vector>

#ifdef _WIN32
#define SHADER_DIR "data/shaders/"
#else
#define SHADER_DIR "../../data/shaders/"
#endif


//////////////////////////////////////////////////////////////////////////
// Utilities

/// Wraps glBufferData to load from a std::vector
/// E.g.:  
/// std::vector< MeshVertex > verts;
/// glBufferDataFromVector( GL_ARRAY_BUFFER, verts, GL_STATIC_DRAW );
template <typename VertexType>
inline void glBufferDataFromVector( GLenum targetBufferObjectType, const std::vector<VertexType>& data, GLenum usagePattern )
{
    glBufferData( targetBufferObjectType, 
                  data.size() * sizeof(VertexType), // size in bytes of entire vertex vector
                  &(data.front()), // address of first elem
                  usagePattern ); 
}

std::string readFileToString( const char* filename );
GLuint createShaderWithErrorHandling( GLuint shaderType, const std::string& shaderSource );
GLuint loadShaderFromFile( const char* vertexShaderFilepath, const char* fragmentShaderFilepath );

// END Utilities
//////////////////////////////////////////////////////////////////////////



/// ShaderAttribute ties together a channel in the vertex data stream 
/// with a name that will be referenced in corresponding shaders.
/// This class is abstract, subclasses are for the concrete data types (e.g. float)
/// TODO  Should be owned by a Shader class
class ShaderAttribute
{
public:
    ShaderAttribute(const std::string& attributeNameInShader,
                           GLsizei attributeSizeInBytes,
                           GLsizei sizeofVertexInBytes,
                           void* offsetVertexAttribute )
    :
    m_name( attributeNameInShader ),
    m_size( attributeSizeInBytes ),
    m_stride( sizeofVertexInBytes ),
    m_offset( offsetVertexAttribute )
    {}
    std::string m_name;
    GLsizei m_size;
    GLsizei m_stride; // sizeof( Vertex )
    void*   m_offset; // offset( Vertex, position/normal/texcoord/... )
    
    virtual ~ShaderAttribute() {}
    void enableByNameInShader( GLuint shaderProgramIndex )
    {
        GLint index = glGetAttribLocation( shaderProgramIndex, m_name.c_str() );  checkOpenGLErrors();
        if( index == -1 )
        {
            std::cerr << "ShaderAttribute::enableByNameInShader() | Failed to find shader attribute of name \""
            << m_name << "\" in shader #" << shaderProgramIndex << "\n";
        } else {
            glEnableVertexAttribArray( index );  checkOpenGLErrors();
        }
    }
    void defineByNameInShader( GLuint shaderProgramIndex )
    {
        GLint index = glGetAttribLocation( shaderProgramIndex, m_name.c_str() );  checkOpenGLErrors();
        if( index == -1 )
        {
            std::cerr << "ShaderAttribute::defineByNameInShader() | Failed to find shader attribute of name \""
            << m_name << "\" in shader #" << shaderProgramIndex << "\n";
        } else {
            assignPointerByIndex( index );
        }
    }
    void disableByNameInShader( GLuint shaderProgramIndex )
    {
        GLint index = glGetAttribLocation( shaderProgramIndex, m_name.c_str() );  checkOpenGLErrors();
        if( index == -1 )
        {
            std::cerr << "UniformShaderAttribute::disableByNameInShader() | Failed to find shader attribute of name \""
            << m_name << "\" in shader #" << shaderProgramIndex << "\n";
        } else {
            glDisableVertexAttribArray( index );  checkOpenGLErrors();
        }
    }
protected:
    
    /// PRE if attribute has an offset, ARRAY_BUFFER must be bound
    virtual void assignPointerByIndex(GLuint attribIndex) = 0;
};
typedef std::shared_ptr<ShaderAttribute> ShaderAttributePtr;

class FloatShaderAttribute : public ShaderAttribute
{
public:
    FloatShaderAttribute(const std::string& attributeNameInShader,
                                GLsizei attributeSizeInBytes,
                                GLsizei sizeofVertexInBytes,
                                void* offsetVertexAttribute )
    : ShaderAttribute(attributeNameInShader,
                             attributeSizeInBytes,
                             sizeofVertexInBytes,
                             offsetVertexAttribute)
    {}
    virtual ~FloatShaderAttribute() {}
    virtual void assignPointerByIndex(GLuint attribIndex)
    {
        //std::cerr << "Assigning float VertexAttribPointer \"" << m_name << "\".\n";
        glVertexAttribPointer( attribIndex, m_size, GL_FLOAT, GL_FALSE, m_stride, m_offset );  checkOpenGLErrors();
    }
};

class UByteShaderAttribute : public ShaderAttribute
{
public:
    UByteShaderAttribute(const std::string& attributeNameInShader,
                                GLsizei attributeSizeInBytes,
                                GLsizei sizeofVertexInBytes,
                                void* offsetVertexAttribute )
    : ShaderAttribute(attributeNameInShader,
                             attributeSizeInBytes,
                             sizeofVertexInBytes,
                             offsetVertexAttribute)
    {}
    virtual ~UByteShaderAttribute() {}
    virtual void assignPointerByIndex(GLuint attribIndex)
    {
        glVertexAttribPointer( attribIndex, m_size, GL_UNSIGNED_BYTE, GL_FALSE, m_stride, m_offset );  checkOpenGLErrors();
    }
};



// Standard vertex type for mesh objects
class MeshVertex
{
public:
    GLfloat m_position[4];
    GLfloat m_normal[4];
    GLfloat m_texCoord[3];
    GLfloat m_diffuseColor[4];
    GLfloat m_shininess;
    GLbyte  m_specular[4];

    static void addShaderAttributes( std::vector<ShaderAttributePtr>&  outShaderAttributes )
    {
        ShaderAttributePtr position(new FloatShaderAttribute("position", 3,
            sizeof(MeshVertex),
            (void*)offsetof(MeshVertex, m_position) )
            );
        outShaderAttributes.push_back( position );
        ShaderAttributePtr diffuse(new FloatShaderAttribute("inVertexColor",
            3,
            sizeof(MeshVertex),
            (void*)offsetof(MeshVertex, m_diffuseColor) )
            );
        outShaderAttributes.push_back( diffuse );
        ShaderAttributePtr texCoord3d(new FloatShaderAttribute("texCoord3d",
            3,
            sizeof(MeshVertex),
            (void*)offsetof(MeshVertex, m_texCoord) )
            );
        outShaderAttributes.push_back( texCoord3d );
    }
};


struct LineVertex
{
    GLfloat m_position[4];
    GLfloat m_direction[4];
    GLfloat m_diffuseColor[4];

    static void addShaderAttributes( std::vector<ShaderAttributePtr>&  outShaderAttributes )
    {
        ShaderAttributePtr position(new FloatShaderAttribute("position", 3,
            sizeof(LineVertex),
            (void*)offsetof(LineVertex, m_position) )
            );
        outShaderAttributes.push_back( position );
        ShaderAttributePtr direction(new FloatShaderAttribute("direction",
            3,
            sizeof(LineVertex),
            (void*)offsetof(LineVertex, m_direction) )
            );
        outShaderAttributes.push_back( direction );
        ShaderAttributePtr diffuse(new FloatShaderAttribute("inColor",
            3,
            sizeof(LineVertex),
            (void*)offsetof(LineVertex, m_diffuseColor) )
            );
        outShaderAttributes.push_back( diffuse );
    }
};




// TODO -- templatize over MeshVertex
/// Mesh supports rendering of a indexed set of triangles
class Mesh : public Renderable
{
public:
    Mesh( const char* vertexShaderFilepath = SHADER_DIR "volumeVertexShader.glsl",
         const char* fragmentShaderFilepath = SHADER_DIR "volumeFragmentShader.glsl" );

    virtual ~Mesh();

    /// Example of changing the mesh geometry.
    virtual void update( float dt );
    virtual void setupRenderState( void );
    virtual void setupShaderState( const RenderContext& renderContext );
    virtual void teardownRenderState( void );
    virtual void render( const RenderContext& renderContext );
    virtual void loadShaders();
    virtual void loadTextures()  {}
    void unitCube();

    /// Construction methods
    static RenderablePtr createBox( void );

protected:
    std::string m_vertexShaderFilepath;
    std::string m_fragmentShaderFilepath;

    GLuint m_dataTextureId;
    GLuint m_vertexArrayObjectId;
    GLuint m_vertexBufferId;
    GLuint m_elementBufferId;
    GLuint m_shaderProgramIndex;
    std::vector< ShaderAttributePtr > m_attributes;
    std::vector< MeshVertex > m_vertexData;
    std::vector< unsigned int > m_vertexIndicies;
    glm::mat4 m_modelTransform;

};


#endif

